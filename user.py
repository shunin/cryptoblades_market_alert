class User:
    def __init__(self, user_address, name=''):
        self.user_address = user_address.lower()
        self.name = name
        self.tracking_ids = []

    def add_tracking_id(self, item_id: str) -> None:
        self.tracking_ids.append(item_id)
