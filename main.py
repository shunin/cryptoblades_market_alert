from user import User
from bsc_parser import Parser
import time


if __name__ == '__main__':
    user_address = '0xeb03B92C3983629E947acDC5d59296E34f33f538'
    acc = User(user_address)
    parser = Parser(acc)

    item_ids = parser.get_token_ids()
    for item_id in item_ids:
        acc.add_tracking_id(item_id)

    s_t = time.time()
    parser.look_for_sales()
    print('work time:', time.time() - s_t)

