import requests
from credentials import TELEGRAM_API, TELEGRAM_CHAT_ID


class Bot:
    def send_message(self, message):
        url = f'https://api.telegram.org/bot{TELEGRAM_API}/sendMessage'
        requests.get(url, params={'chat_id': TELEGRAM_CHAT_ID, 'text': message})
