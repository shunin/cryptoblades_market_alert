import requests
import time

from bs4 import BeautifulSoup
from user import User
from bot import Bot

from credentials import BSCSCAN_API_TOKEN


class Parser:
    bot = Bot()
    def __init__(self, user: User):
        self.user = user

    def look_for_sales(self):
        while self.user.tracking_ids:
            txs = self.get_erc721_txs()
            for item in self.user.tracking_ids:
                if not self.item_listed(item, txs):
                    self.user.tracking_ids.remove(item)
                    print(f'Item {item} is not listed on market')
                else:
                    print(f'Item {item} is listed')
                    price = self.get_sell_price(item)
                    if price:
                        print(f'Item {item} was sold!')
                        self.bot.send_message(f'Item {item} was sold for {price} SKILL')
                        self.user.tracking_ids.remove(item)

    def get_sell_price(self, item_id: str):
        txs = self.get_bep20_txs()
        CONTRACT_ADDRESS = '0x154a9f9cbd3449ad22fdae23044319d6ef2a1fab'
        txs_checked = 0
        for tx in txs['result']:
            if tx['to'] == self.user.user_address and tx['contractAddress'] == CONTRACT_ADDRESS:
                if self.item_sold_transaction(tx['hash'], item_id):
                    price = float(tx['value']) / pow(10, float(tx['tokenDecimal']))
                    return str(price)[:5]
            txs_checked += 1
            time.sleep(0.5)
            #if txs_checked > 10:
            #    return False
        return False

    def item_sold_transaction(self, tx: str, item_id: str) -> bool:
        url = 'https://bscscan.com/tx/'
        tx_page = requests.get(url + tx)
        soup = BeautifulSoup(tx_page.text, features='lxml')
        tx_item_id = soup.find('span', class_='ml-1 mr-1')
        if tx_item_id:
            tx_item_id = tx_item_id.find('a').text
        if item_id == tx_item_id:
            return True
        return False

    def get_bep20_txs(self):
        url = f"https://api.bscscan.com/api?module=account&action=tokentx&address={self.user.user_address}&startblock=9000000&endblock=20000000&sort=desc&apikey={BSCSCAN_API_TOKEN}"
        return requests.get(url).json()

    def item_listed(self, item_id: str, txs: dict) -> bool:
        for tx in txs['result']:
            if tx['from'] == self.user.user_address and tx['tokenID'] == item_id:
                return True
        return False

    def get_erc721_txs(self) -> dict:
        url = f"https://api.bscscan.com/api?module=account&action=tokennfttx&address={self.user.user_address}&startblock=9000000&endblock=999999999&sort=desc&apikey={BSCSCAN_API_TOKEN}"
        return requests.get(url).json()

    def get_token_ids(self) -> list:
        token_ids = []
        txs = self.get_erc721_txs()
        print('Getting token ids..')
        for tx in txs['result']:
            if tx['to'] == self.user.user_address:
                token_ids.append(tx['tokenID'])
        return token_ids

